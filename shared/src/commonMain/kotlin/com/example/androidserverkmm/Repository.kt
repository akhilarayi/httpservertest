package com.example.androidserverkmm

import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.SerialName


object Repository {

    const val BASE_URL = "http://192.168.43.67:8080/api/1.0/"
    val PROFILE_URL = "profile"
    val USERS_URL = "retrieveuser"
    val ALL_USERS_URL = "retrieveallusers"
    const val ADD_USER = "registeruser"

    val httpClient: HttpClient = HttpClient {

        install(JsonFeature) {
            val json = kotlinx.serialization.json.Json {
                ignoreUnknownKeys = true
                isLenient = true
            }
            serializer = KotlinxSerializer(json)
        }
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.ALL
        }
    }

    @Throws(Exception::class)
    suspend fun getProfile(): Profile {

        return httpClient.get(BASE_URL + PROFILE_URL)
    }

    @Throws(Exception::class)
    suspend fun getUsersfromdb(str_username: String): List<UserDetails> {

        return httpClient.post(BASE_URL + USERS_URL) {

            parameter("username", str_username)
        }
    }

    @Throws(Exception::class)
    suspend fun getAllUsersfromdb(): List<UserDetails> {

        return httpClient.post(BASE_URL + ALL_USERS_URL)
    }

    @Throws(Exception::class)
    suspend fun addUser(userDetails: UserDetails): GenericResponse {

        return httpClient.submitForm {

            url {

                URLBuilder("${BASE_URL}${ADD_USER}")
            }

            formData {

                Parameters.build {
                    append("username", userDetails.username)
                    append("password", userDetails.password)
                    append("mobilenumber", userDetails.mobilenumber)
                }
            }
        }
    }

}

@Serializable
data class Profile(
    @SerialName("message")
    val message: String,
    @SerialName("result")
    val result: List<Result>,
    @SerialName("status")
    val status: Int
)

@Serializable
data class Result(
    @SerialName("company")
    val company: String,
    @SerialName("id")
    val id: String,
    @SerialName("name")
    val name: String
)


@Serializable
data class UserDetails(
    @SerialName("mobilenumber")
    val mobilenumber: String,
    @SerialName("password")
    val password: String,
    @SerialName("username")
    val username: String
)

@Serializable
data class GenericResponse(

    @SerialName("status")
    val status: Int,
    @SerialName("message")
    val message: String
)
