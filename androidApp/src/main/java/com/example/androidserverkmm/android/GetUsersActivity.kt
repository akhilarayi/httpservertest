package com.example.androidserverkmm.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.androidserverkmm.Repository
import com.google.gson.Gson
import kotlinx.coroutines.launch

class GetUsersActivity : AppCompatActivity() {

    lateinit var tv_response: TextView
    lateinit var progressbar: ProgressBar
    lateinit var btn: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_users)
        btn = findViewById(R.id.btn_getallusers)
        progressbar = findViewById(R.id.progressbar_getallusers)
        tv_response = findViewById(R.id.tv_response_getallusers)

        btn.setOnClickListener {

            getAllUsersfromdb()

        }
    }

    fun getAllUsersfromdb() {

        progressbar.visibility = View.VISIBLE
        btn.visibility = View.GONE

        lifecycleScope.launch {

            kotlin.runCatching {

                Repository.getAllUsersfromdb()

            }.onSuccess {

                val result = Gson().toJson(it)
                Log.e("@@@ success-->", result)
                progressbar.visibility = View.GONE
                btn.visibility = View.VISIBLE
                tv_response.setText(result)

            }.onFailure {

                Log.e("@@@ error-->", it.message.toString())
                progressbar.visibility = View.GONE
                btn.visibility = View.VISIBLE
                Toast.makeText(this@GetUsersActivity, it.message, Toast.LENGTH_SHORT).show()

            }
        }
    }
}