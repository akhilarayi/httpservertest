package com.example.androidserverkmm.android

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.androidserverkmm.Repository
import com.google.gson.Gson
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    lateinit var tv_response: TextView
    lateinit var progressbar: ProgressBar
    lateinit var btn: Button
    lateinit var etPersonName: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btn = findViewById(R.id.btn_getusers)
        progressbar = findViewById(R.id.progressbar)
        tv_response = findViewById(R.id.tv_response)
        etPersonName = findViewById(R.id.etPersonName)

        btn.setOnClickListener {

            // getProfile()
            if (etPersonName.text.toString().isNotEmpty()) {
                getUsersfromdb()
            } else {
                Toast.makeText(this, "Please enter Person Name", Toast.LENGTH_SHORT).show()
            }
        }

    }

    fun getProfile() {

        progressbar.visibility = View.VISIBLE
        btn.visibility = View.GONE

        lifecycleScope.launch {

            kotlin.runCatching {

                Repository.getProfile()

            }.onSuccess {

                Log.e("@@@ success-->", it.toString())
                progressbar.visibility = View.GONE
                btn.visibility = View.VISIBLE
                tv_response.setText(it.result[0].id + "," + it.result[0].name + "," + it.result[0].company)

            }.onFailure {

                Log.e("@@@ error-->", it.cause.toString())
                progressbar.visibility = View.GONE
                btn.visibility = View.VISIBLE
                Toast.makeText(this@MainActivity, it.message, Toast.LENGTH_SHORT).show()

            }
        }
    }

    fun getUsersfromdb() {

        progressbar.visibility = View.VISIBLE
        btn.visibility = View.GONE

        lifecycleScope.launch {

            kotlin.runCatching {

                Repository.getUsersfromdb(etPersonName.text.toString())

            }.onSuccess {

                val result = Gson().toJson(it)
                Log.e("@@@ success-->", result)
                progressbar.visibility = View.GONE
                btn.visibility = View.VISIBLE
                tv_response.setText(result)

            }.onFailure {

                Log.e("@@@ error-->", it.message.toString())
                progressbar.visibility = View.GONE
                btn.visibility = View.VISIBLE
                Toast.makeText(this@MainActivity, it.message, Toast.LENGTH_SHORT).show()

            }
        }
    }
}
