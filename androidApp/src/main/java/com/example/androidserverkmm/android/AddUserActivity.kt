package com.example.androidserverkmm.android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import com.example.androidserverkmm.Repository
import com.example.androidserverkmm.UserDetails
import com.google.gson.Gson
import kotlinx.coroutines.launch

class AddUserActivity : AppCompatActivity() {

    lateinit var et_userName: EditText
    lateinit var et_password: EditText
    lateinit var et_mobilenumber: EditText
    lateinit var progressbar:ProgressBar
    lateinit var btn_adduser:Button



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)
        et_userName=findViewById(R.id.et_userName)
        et_password=findViewById(R.id.et_password)
        et_mobilenumber=findViewById(R.id.et_mobilenumber)
        progressbar=findViewById(R.id.progressbar_adduser)
        btn_adduser=findViewById(R.id.btn_adduser)

        btn_adduser.setOnClickListener {

            val s_username:String=et_userName.text.toString()
            val s_password:String=et_password.text.toString()
            val s_mobilenumber:String=et_mobilenumber.text.toString()

            if(s_username.isEmpty() || s_password.isEmpty() || s_mobilenumber.isEmpty()){

                Toast.makeText(this,"Please fill all the details",Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            val userDetails=UserDetails(s_username,s_password,s_mobilenumber)
            addUser(userDetails)
        }
    }

    fun addUser(userDetails: UserDetails) {

        progressbar.visibility = View.VISIBLE
        btn_adduser.visibility = View.GONE

        lifecycleScope.launch {

            kotlin.runCatching {

                Repository.addUser(userDetails)

            }.onSuccess {

                val result = Gson().toJson(it)
                Log.e("@@@ success-->", result)
                progressbar.visibility = View.GONE
                btn_adduser.visibility = View.VISIBLE


            }.onFailure {

                Log.e("@@@ error-->", it.message.toString())
                progressbar.visibility = View.GONE
                btn_adduser.visibility = View.VISIBLE
                Toast.makeText(this@AddUserActivity, it.message, Toast.LENGTH_SHORT).show()

            }
        }
    }
}