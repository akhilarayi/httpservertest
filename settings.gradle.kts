pluginManagement {
    repositories {
        google()
        gradlePluginPortal()
        mavenCentral()
    }
}

rootProject.name = "TestAndroidServerKMM"
include(":androidApp")
include(":shared")